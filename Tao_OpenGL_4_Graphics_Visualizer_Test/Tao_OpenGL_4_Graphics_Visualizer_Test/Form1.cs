﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tao.FreeGlut;
using Tao.OpenGl;
using Tao.Platform;

namespace Tao_OpenGL_4_Graphics_Visualizer_Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            AnT.InitializeContexts();
        }

        double ScreenW, ScreenH; //Размеры окна
        private float devX, devY; //Отношения сторон окна визуализации
        private float[,] GrapValueArray; //Хранит x, y точки графика
        private int elements_count = 0; //Количество элементов в массиве
        private bool not_calculate = true; //Заполнен ли массив графика
        private int pointPosition = 0; //Номер ячейки массива красной точки
        float lineX, lineY; //Вспомогательные переменные
        float Mcoord_X = 0, Mcoord_Y = 0; //Текущие координаты курсора мыши

        private void AnT_MouseMove(object sender, MouseEventArgs e)
        {
            Mcoord_X = e.X;
            Mcoord_Y = e.Y;
            lineX = devX * e.X;
            lineY = (float)(ScreenH - devY * e.Y);
            //lineY = functionY(e.X);
        }
        private void PointInGrap_Tick(object sender, EventArgs e)
        {
            if(pointPosition == elements_count - 1)
            {
                pointPosition = 0;
            }
            Draw();
            pointPosition++;
        }
        private void PrintText2D(float x, float y, string text)
        {
            Gl.glRasterPos2f(x, y);
            foreach(char char_for_draw in text)
            {
                Glut.glutBitmapCharacter(Glut.GLUT_BITMAP_9_BY_15, char_for_draw); 
            }
        }
        private float functionY(float x)
        {
            return (float)Math.Sin(x) * 3 + 1;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            Gl.glViewport(0, 0, AnT.Width, AnT.Height);
            //Сохранение коэ-ов для перевода координат оконной системы в координаты OpenGL
            devX = (float)ScreenW / (float)AnT.Width;
            devY = (float)ScreenH / (float)AnT.Height;
        }

        private void functionCalculation()
        {
            float x = 0, y = 0;
            GrapValueArray = new float[300, 2];
            elements_count = 0;
            for (x = -15; x < 15; x += 0.1f)
            {
                y = functionY(x);
                //y = (float)Math.Sin(x) * 3 + 1;
                GrapValueArray[elements_count, 0] = x;
                GrapValueArray[elements_count, 1] = y;
                elements_count++;
            }
            not_calculate = false;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Glut.glutInit();
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_SINGLE);
            Gl.glClearColor(255, 255, 255, 1);
            Gl.glViewport(0, 0, AnT.Width, AnT.Height);
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            if ((float)AnT.Width <= (float)AnT.Height)
            {
                ScreenW = 30.0;
                ScreenH = 30.0 * (float)(float)AnT.Height / (float)AnT.Width;
                Glu.gluOrtho2D(0.0, ScreenW, 0.0, ScreenH);
            }
            else
            {
                ScreenH = 30.0;
                ScreenW = 30.0 * (float)(float)AnT.Height / (float)AnT.Width;
                Glu.gluOrtho2D(0.0, ScreenW, 0.0, ScreenH);
            }
            //Сохранение коэ-ов для перевода координат оконной системы в координаты OpenGL
            devX = (float)ScreenW / (float)AnT.Width;
            devY = (float)ScreenH / (float)AnT.Height;
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            PointInGrap.Start();
        }
        private void DrawDiagram()
        {
            if (not_calculate)
            {
                functionCalculation();
            }
            Gl.glBegin(Gl.GL_LINE_STRIP);
            Gl.glVertex2d(GrapValueArray[0, 0], GrapValueArray[0, 1]);
            for(int ax = 1; ax < elements_count; ax += 2)
            {
                Gl.glVertex2d(GrapValueArray[ax, 0], GrapValueArray[ax, 1]);
            }
            Gl.glEnd();
            Gl.glPointSize(5);
            Gl.glColor3f(255, 0, 0);
            Gl.glBegin(Gl.GL_POINTS);
            Gl.glVertex2d(GrapValueArray[pointPosition, 0], GrapValueArray[pointPosition, 1]);
            Gl.glEnd();
            Gl.glPointSize(1);
        }
        private void Draw()
        {
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glLoadIdentity();
            Gl.glColor3f(0, 0, 0);
            Gl.glPushMatrix();
            Gl.glTranslated(15, 15, 0);
            Gl.glBegin(Gl.GL_POINTS);
            for(int ax = -15; ax < 15; ax++)
            {
                for(int bx = -15; bx < 15; bx++)
                {
                    Gl.glVertex2d(ax, bx);
                }
            }
            Gl.glEnd();
            Gl.glBegin(Gl.GL_LINES);

            Gl.glVertex2d(0, -15);
            Gl.glVertex2d(0, 15);
            Gl.glVertex2d(-15, 0);
            Gl.glVertex2d(15, 0);

            Gl.glVertex2d(0, 15);
            Gl.glVertex2d(0.1, 14.5);
            Gl.glVertex2d(0, 15);
            Gl.glVertex2d(-0.1, 14.5);

            Gl.glVertex2d(15, 0);
            Gl.glVertex2d(14.5, 0.1);
            Gl.glVertex2d(15, 0);
            Gl.glVertex2d(14.5, -0.1);

            Gl.glEnd();
            PrintText2D(15.5f, 0, "x");
            PrintText2D(0.5f, 14.5f, "y");
            DrawDiagram();
            Gl.glPopMatrix();
            if (lineX  <= 12)
            {
                PrintText2D(lineX + 0.2f, functionY(lineX - 15) + 15 + 0.4f, "[ x: "
                + (lineX - 15).ToString() + " y: " + (functionY(lineX - 15)).ToString() + "]");
            }
            if (lineX > 12)
            {
                PrintText2D(0, 0 + 0.4f, "[ x: "
                + (lineX - 15).ToString() + " y: " + (functionY(lineX - 15)).ToString() + "]");
            }
            
            /*PrintText2D(devX * Mcoord_X + 0.2f, (float)ScreenH - devY * Mcoord_Y + 0.4f, "[ x: "
                + (devX * Mcoord_X - 15).ToString() + " y: " + ((float)ScreenH - devY * Mcoord_Y - 15).ToString() + "]");*/

            Gl.glColor3f(255, 0, 0);
            Gl.glBegin(Gl.GL_LINES);

            /*
            Gl.glVertex2d(lineX, 15);
            Gl.glVertex2d(lineX, lineY);
            Gl.glVertex2d(15, lineY);
            Gl.glVertex2d(lineX, lineY);*/
            Gl.glVertex2d(lineX, 15);
            Gl.glVertex2d(lineX, functionY(lineX - 15) + 15);
            Gl.glVertex2d(15, functionY(lineX - 15) + 15);
            Gl.glVertex2d(lineX, functionY(lineX - 15) + 15);

            

            Gl.glEnd();
            Gl.glFlush();
            AnT.Invalidate();
        }
    }
}
